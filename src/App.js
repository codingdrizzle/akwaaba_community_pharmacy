import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import "./App.css";
import Home from "./Components/Home";
import NavBar from "./Components/NavBar";
import Footer from "./Components/Footer";
import About from "./Components/About";
import NewsFeed from "./Components/NewsFeed";
import HealthTips from "./Components/HealthTips";
import Contact from "./Components/Contact";
import Blog from "../src/BlogComponents/Blog";

function App() {
  return (
    // <Router>
    //   <div>
    //     <NavBar />
    //     <Switch>
    //       <Route path="/" exact component={Home} />
    //       <Route path="/about" component={About} />
    //       <Route path="/news-feed" component={NewsFeed} />
    //       <Route path="/health-tips" component={HealthTips} />
    //       <Route path="/contact" component={Contact} />
    //     {/* <Route path="/blog" component={Blog} /> */}
    //       <Home />
    //     </Switch>
    //     <Footer />
    //   </div>
    // </Router>
        <Blog/>
  );
}

export default App;
